/**
 * 
 * Written by M�rton M�nz and Philip C Biggin
 * Copyright (c) University of Oxford, United Kingdom
 * Visit http://sbcb.bioch.ox.ac.uk/jgromacs/
 * 
 * This source code file is part of JGromacs v1.0.
 * 
 * JGromacs v1.0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JGromacs v1.0. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JGromacs v1.0. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package jgromacs.db;

/**
 * Objects of this class represent an atom type
 *
 */
public class AtomType implements Cloneable{

	private int which = 0;
	
	// Constants: the full periodic table
	
	final static int Hydrogen = 1;
	final static int Helium = 2;
	final static int Lithium = 3;
	final static int Beryllium = 4;
	final static int Boron = 5;
	final static int Carbon = 6;
	final static int Nitrogen = 7;
	final static int Oxygen = 8;
	final static int Fluorine = 9;
	final static int Neon = 10;
	final static int Sodium = 11;
	final static int Magnesium = 12;
	final static int Aluminum = 13;
	final static int Silicon = 14;
	final static int Phosphorus = 15;
	final static int Sulfur = 16;
	final static int Chlorine = 17;
	final static int Argon = 18;
	final static int Potassium = 19;
	final static int Calcium = 20;
	final static int Scandium = 21;
	final static int Titanium = 22;
	final static int Vanadium = 23;
	final static int Chromium = 24;
	final static int Manganese = 25;
	final static int Iron = 26;
	final static int Cobalt = 27;
	final static int Nickel = 28;
	final static int Copper = 29;
	final static int Zinc = 30;
	final static int Gallium = 31;
	final static int Germanium = 32;
	final static int Arsenic = 33;
	final static int Selenium = 34;
	final static int Bromine = 35;
	final static int Krypton = 36;
	final static int Rubidium = 37;
	final static int Strontium = 38;
	final static int Yttrium = 39;
	final static int Zirconium = 40;
	final static int Niobium = 41;
	final static int Molybdenum = 42;
	final static int Technetium = 43;
	final static int Ruthenium = 44;
	final static int Rhodium = 45;
	final static int Palladium = 46;
	final static int Silver = 47;
	final static int Cadmium = 48;
	final static int Indium = 49;
	final static int Tin = 50;
	final static int Antimony = 51;
	final static int Tellurium = 52;
	final static int Iodine = 53;
	final static int Xenon = 54;
	final static int Cesium = 55;
	final static int Barium = 56;
	final static int Lanthanum = 57;
	final static int Cerium = 58;
	final static int Praseodymium = 59;
	final static int Neodymium = 60;
	final static int Promethium = 61;
	final static int Samarium = 62;
	final static int Europium = 63;
	final static int Gadolinium = 64;
	final static int Terbium = 65;
	final static int Dysprosium = 66;
	final static int Holmium = 67;
	final static int Erbium = 68;
	final static int Thulium = 69;
	final static int Ytterbium = 70;
	final static int Lutetium = 71;
	final static int Hafnium = 72;
	final static int Tantalum = 73;
	final static int Tungsten = 74;
	final static int Rhenium = 75;
	final static int Osmium = 76;
	final static int Iridium = 77;
	final static int Platinum = 78;
	final static int Gold = 79;
	final static int Mercury = 80;
	final static int Thallium = 81;
	final static int Lead = 82;
	final static int Bismuth = 83;
	final static int Polonium = 84;
	final static int Astatine = 85;
	final static int Radon = 86;
	final static int Francium = 87;
	final static int Radium = 88;
	final static int Actinium = 89;
	final static int Thorium = 90;
	final static int Protactinium = 91;
	final static int Uranium = 92;
	final static int Neptunium = 93;
	final static int Plutonium = 94;
	final static int Americium = 95;
	final static int Curium = 96;
	final static int Berkelium = 97;
	final static int Californium = 98;
	final static int Einsteinium = 99;
	final static int Fermium = 100;
	final static int Mendelevium = 101;
	final static int Nobelium = 102;
	final static int Lawrencium = 103;
	final static int Rutherfordium = 104;
	final static int Dubnium = 105;
	final static int Seaborgium = 106;
	final static int Bohrium = 107;
	final static int Hassium = 108;
	final static int Meitnerium = 109;

	// Constructors
	
	/**
	 * Constructs a new AtomType object
	 *
	 * 
	 */
	public AtomType() {
		
	}
	
	/**
	 * Constructs a new AtomType object of a given type
	 * @param type atom type
	 * 
	 */
	public AtomType(int type){
		which = type;
	}
	
	/**
	 * Constructs a new AtomType object of given code
	 * @param code atom code
	 * 
	 */
	public AtomType(String code){
		code = code.toUpperCase();
		if (code.equals("H")) which = 1;
		if (code.equals("HE")) which = 2;
		if (code.equals("LI")) which = 3;
		if (code.equals("BE")) which = 4;
		if (code.equals("B")) which = 5;
		if (code.equals("C")) which = 6;
		if (code.equals("N")) which = 7;
		if (code.equals("O")) which = 8;
		if (code.equals("F")) which = 9;
		if (code.equals("NE")) which = 10;
		if (code.equals("NA")) which = 11;
		if (code.equals("MG")) which = 12;
		if (code.equals("AL")) which = 13;
		if (code.equals("SI")) which = 14;
		if (code.equals("P")) which = 15;
		if (code.equals("S")) which = 16;
		if (code.equals("CL")) which = 17;
		if (code.equals("AR")) which = 18;
		if (code.equals("K")) which = 19;
		if (code.equals("CA")) which = 20;
		if (code.equals("SC")) which = 21;
		if (code.equals("TI")) which = 22;
		if (code.equals("V")) which = 23;
		if (code.equals("CR")) which = 24;
		if (code.equals("MN")) which = 25;
		if (code.equals("FE")) which = 26;
		if (code.equals("CO")) which = 27;
		if (code.equals("NI")) which = 28;
		if (code.equals("CU")) which = 29;
		if (code.equals("ZN")) which = 30;
		if (code.equals("GA")) which = 31;
		if (code.equals("GE")) which = 32;
		if (code.equals("AS")) which = 33;
		if (code.equals("SE")) which = 34;
		if (code.equals("BR")) which = 35;
		if (code.equals("KR")) which = 36;
		if (code.equals("RB")) which = 37;
		if (code.equals("SR")) which = 38;
		if (code.equals("Y")) which = 39;
		if (code.equals("ZR")) which = 40;
		if (code.equals("NB")) which = 41;
		if (code.equals("MO")) which = 42;
		if (code.equals("TC")) which = 43;
		if (code.equals("RU")) which = 44;
		if (code.equals("RH")) which = 45;
		if (code.equals("PD")) which = 46;
		if (code.equals("AG")) which = 47;
		if (code.equals("CD")) which = 48;
		if (code.equals("IN")) which = 49;
		if (code.equals("SN")) which = 50;
		if (code.equals("SB")) which = 51;
		if (code.equals("TE")) which = 52;
		if (code.equals("I")) which = 53;
		if (code.equals("XE")) which = 54;
		if (code.equals("CS")) which = 55;
		if (code.equals("BA")) which = 56;
		if (code.equals("LA")) which = 57;
		if (code.equals("CE")) which = 58;
		if (code.equals("PR")) which = 59;
		if (code.equals("ND")) which = 60;
		if (code.equals("PM")) which = 61;
		if (code.equals("SM")) which = 62;
		if (code.equals("EU")) which = 63;
		if (code.equals("GD")) which = 64;
		if (code.equals("TB")) which = 65;
		if (code.equals("DY")) which = 66;
		if (code.equals("HO")) which = 67;
		if (code.equals("ER")) which = 68;
		if (code.equals("TM")) which = 69;
		if (code.equals("YB")) which = 70;
		if (code.equals("LU")) which = 71;
		if (code.equals("HF")) which = 72;
		if (code.equals("TA")) which = 73;
		if (code.equals("W")) which = 74;
		if (code.equals("RE")) which = 75;
		if (code.equals("OS")) which = 76;
		if (code.equals("IR")) which = 77;
		if (code.equals("PT")) which = 78;
		if (code.equals("AU")) which = 79;
		if (code.equals("HG")) which = 80;
		if (code.equals("TL")) which = 81;
		if (code.equals("PB")) which = 82;
		if (code.equals("BI")) which = 83;
		if (code.equals("PO")) which = 84;
		if (code.equals("AT")) which = 85;
		if (code.equals("RN")) which = 86;
		if (code.equals("FR")) which = 87;
		if (code.equals("RA")) which = 88;
		if (code.equals("AC")) which = 89;
		if (code.equals("TH")) which = 90;
		if (code.equals("PA")) which = 91;
		if (code.equals("U")) which = 92;
		if (code.equals("NP")) which = 93;
		if (code.equals("PU")) which = 94;
		if (code.equals("AM")) which = 95;
		if (code.equals("CM")) which = 96;
		if (code.equals("BK")) which = 97;
		if (code.equals("CF")) which = 98;
		if (code.equals("ES")) which = 99;
		if (code.equals("FM")) which = 100;
		if (code.equals("MD")) which = 101;
		if (code.equals("NO")) which = 102;
		if (code.equals("LR")) which = 103;
		if (code.equals("RF")) which = 104;
		if (code.equals("DB")) which = 105;
		if (code.equals("SG")) which = 106;
		if (code.equals("BH")) which = 107;
		if (code.equals("HS")) which = 108;
		if (code.equals("MT")) which = 109;
	}
	
	// Getters
	
	/**
	 * Returns the code of atom type
	 * @return atom code
	 * 
	 */
	public String getCode(){
		if (which==1) return "H";
		if (which==2) return "He";
		if (which==3) return "Li";
		if (which==4) return "Be";
		if (which==5) return "B";
		if (which==6) return "C";
		if (which==7) return "N";
		if (which==8) return "O";
		if (which==9) return "F";
		if (which==10) return "Ne";
		if (which==11) return "Na";
		if (which==12) return "Mg";		
		if (which==13) return "Al";
		if (which==14) return "Si";
		if (which==15) return "P";
		if (which==16) return "S";
		if (which==17) return "Cl";
		if (which==18) return "Ar";
		if (which==19) return "K";
		if (which==20) return "Ca";
		if (which==21) return "Sc";
		if (which==22) return "Ti";
		if (which==23) return "V";
		if (which==24) return "Cr";
		if (which==25) return "Mn";
		if (which==26) return "Fe";
		if (which==27) return "Co";
		if (which==28) return "Ni";
		if (which==29) return "Cu";
		if (which==30) return "Zn";
		if (which==31) return "Ga";
		if (which==32) return "Ge";
		if (which==33) return "As";
		if (which==34) return "Se";
		if (which==35) return "Br";
		if (which==36) return "Kr";
		if (which==37) return "Rb";
		if (which==38) return "Sr";
		if (which==39) return "Y";
		if (which==40) return "Zr";
		if (which==41) return "Nb";
		if (which==42) return "Mo";
		if (which==43) return "Tc";
		if (which==44) return "Ru";
		if (which==45) return "Rh";
		if (which==46) return "Pd";
		if (which==47) return "Ag";
		if (which==48) return "Cd";
		if (which==49) return "In";
		if (which==50) return "Sn";
		if (which==51) return "Sb";
		if (which==52) return "Te";
		if (which==53) return "I";
		if (which==54) return "Xe";
		if (which==55) return "Cs";
		if (which==56) return "Ba";
		if (which==57) return "La";
		if (which==58) return "Ce";
		if (which==59) return "Pr";
		if (which==60) return "Nd";
		if (which==61) return "Pm";
		if (which==62) return "Sm";
		if (which==63) return "Eu";
		if (which==64) return "Gd";
		if (which==65) return "Tb";
		if (which==66) return "Dy";
		if (which==67) return "Ho";
		if (which==68) return "Er";
		if (which==69) return "Tm";
		if (which==70) return "Yb";
		if (which==71) return "Lu";
		if (which==72) return "Hf";
		if (which==73) return "Ta";
		if (which==74) return "W";
		if (which==75) return "Re";
		if (which==76) return "Os";
		if (which==77) return "Ir";
		if (which==78) return "Pt";
		if (which==79) return "Au";
		if (which==80) return "Hg";
		if (which==81) return "Tl";
		if (which==82) return "Pb";
		if (which==83) return "Bi";
		if (which==84) return "Po";	
		if (which==85) return "At";
		if (which==86) return "Rn";
		if (which==87) return "Fr";
		if (which==88) return "Ra";
		if (which==89) return "Ac";
		if (which==90) return "Th";
		if (which==91) return "Pa";
		if (which==92) return "U";
		if (which==93) return "Np";
		if (which==94) return "Pu";
		if (which==85) return "At";
		if (which==86) return "Rn";
		if (which==87) return "Fr";
		if (which==88) return "Ra";
		if (which==89) return "Ac";
		if (which==90) return "Th";
		if (which==91) return "Pa";
		if (which==92) return "U";
		if (which==93) return "Np";
		if (which==94) return "Pu";
		if (which==95) return "Am";
		if (which==96) return "Cm";
		if (which==97) return "Bk";
		if (which==98) return "Cf";
		if (which==99) return "Es";
		if (which==100) return "Fm";
		if (which==101) return "Md";
		if (which==102) return "No";
		if (which==103) return "Lr";
		if (which==104) return "Rf";	
		if (which==105) return "Db";
		if (which==106) return "Sg";
		if (which==107) return "Bh";
		if (which==108) return "Hs";
		if (which==109) return "Mt";
		
		return "";
	}
	
	/**
	 * Returns the full name of atom type
	 * @return atom name
	 * 
	 */
	public String getFullName(){
		if (which==1) return "Hydrogen";
		if (which==2) return "Helium";
		if (which==3) return "Lithium";
		if (which==4) return "Beryllium";
		if (which==5) return "Boron";
		if (which==6) return "Carbon";
		if (which==7) return "Nitrogen";
		if (which==8) return "Oxygen";
		if (which==9) return "Fluorine";
		if (which==10) return "Neon";
		if (which==11) return "Sodium";
		if (which==12) return "Magnesium";
		if (which==13) return "Aluminum";
		if (which==14) return "Silicon";
		if (which==15) return "Phosphorus";
		if (which==16) return "Sulfur";
		if (which==17) return "Chlorine";
		if (which==18) return "Argon";
		if (which==19) return "Potassium";
		if (which==20) return "Calcium";
		if (which==21) return "Scandium";
		if (which==22) return "Titanium";
		if (which==23) return "Vanadium";
		if (which==24) return "Chromium";
		if (which==25) return "Manganese";
		if (which==26) return "Iron";
		if (which==27) return "Cobalt";
		if (which==28) return "Nickel";
		if (which==29) return "Copper";
		if (which==30) return "Zinc";
		if (which==31) return "Gallium";
		if (which==32) return "Germanium";
		if (which==33) return "Arsenic";
		if (which==34) return "Selenium";
		if (which==35) return "Bromine";
		if (which==36) return "Krypton";
		if (which==37) return "Rubidium";
		if (which==38) return "Strontium";
		if (which==39) return "Yttrium";
		if (which==40) return "Zirconium";
		if (which==41) return "Niobium";
		if (which==42) return "Molybdenum";
		if (which==43) return "Technetium";
		if (which==44) return "Ruthenium";
		if (which==45) return "Rhodium";
		if (which==46) return "Palladium";
		if (which==47) return "Silver";
		if (which==48) return "Cadmium";
		if (which==49) return "Indium";
		if (which==50) return "Tin";
		if (which==51) return "Antimony";
		if (which==52) return "Tellurium";
		if (which==53) return "Iodine";
		if (which==54) return "Xenon";
		if (which==55) return "Cesium";
		if (which==56) return "Barium";
		if (which==57) return "Lanthanum";
		if (which==58) return "Cerium";
		if (which==59) return "Praseodymium";
		if (which==60) return "Neodymium";
		if (which==61) return "Promethium";
		if (which==62) return "Samarium";
		if (which==63) return "Europium";
		if (which==64) return "Gadolinium";
		if (which==65) return "Terbium";
		if (which==66) return "Dysprosium";
		if (which==67) return "Holmium";
		if (which==68) return "Erbium";
		if (which==69) return "Thulium";
		if (which==70) return "Ytterbium";
		if (which==71) return "Lutetium";
		if (which==72) return "Hafnium";
		if (which==73) return "Tantalum";
		if (which==74) return "Tungsten";
		if (which==75) return "Rhenium";
		if (which==76) return "Osmium";
		if (which==77) return "Iridium";
		if (which==78) return "Platinum";
		if (which==79) return "Gold";
		if (which==80) return "Mercury";
		if (which==81) return "Thallium";
		if (which==82) return "Lead";
		if (which==83) return "Bismuth";
		if (which==84) return "Polonium";
		if (which==85) return "Astatine";
		if (which==86) return "Radon";
		if (which==87) return "Francium";
		if (which==88) return "Radium";
		if (which==89) return "Actinium";
		if (which==90) return "Thorium";
		if (which==91) return "Protactinium";
		if (which==92) return "Uranium";
		if (which==93) return "Neptunium";
		if (which==94) return "Plutonium";
		if (which==95) return "Americium";
		if (which==96) return "Curium";
		if (which==97) return "Berkelium";
		if (which==98) return "Californium";
		if (which==99) return "Einsteinium";
		if (which==100) return "Fermium";
		if (which==101) return "Mendelevium";
		if (which==102) return "Nobelium";
		if (which==103) return "Lawrencium";
		if (which==104) return "Rutherfordium";
		if (which==105) return "Dubnium";
		if (which==106) return "Seaborgium";
		if (which==107) return "Bohrium";
		if (which==108) return "Hassium";
		if (which==109) return "Meitnerium";
		return "";
	}
	
	// toString, clone, equals
	
	/**
	 * Returns the String representation of atom type
	 *
	 * @return String representation
	 */
	public String toString(){
		return getCode();
	}
	
	/**
	 * Returns an identical AtomType object 
	 *
	 * @return clone of the atom type
	 */
	public Object clone(){
		try {
			AtomType ret = (AtomType)super.clone();
			ret.which = which;
			return ret;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	/**
	 * Returns true if the two atom types are identical
	 * @param other the other atom type
	 * 
	 */
	public boolean equals(Object other){
		if (other == null) return false;
		if (other == this) return true;
		if (this.getClass() != other.getClass()) return false;
		AtomType type = (AtomType)other;
		if (type.which==which) return true;
		else return false;
	}
	
	/**
	 * Returns hash code
	 * 
	 */
	public int hashCode() {
		return which;
	}
	
}
