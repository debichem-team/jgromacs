/**
 * 
 * Written by M�rton M�nz and Philip C Biggin
 * Copyright (c) University of Oxford, United Kingdom
 * Visit http://sbcb.bioch.ox.ac.uk/jgromacs/
 * 
 * This source code file is part of JGromacs v1.0.
 * 
 * JGromacs v1.0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JGromacs v1.0. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JGromacs v1.0. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package jgromacs.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Objects of this class represent a single frame index set
 *
 */
public class FrameIndexSet implements Cloneable {

	private String name = "Noname";
	private TreeSet<Integer> frame_indices = new TreeSet<Integer>();
	
	// Constructors
	
	/**
	 * Constructs a new FrameIndexSet object
	 *
	 * 
	 */
	public FrameIndexSet() {
		
	}
	
	/**
	 * Constructs a new FrameIndexSet object of a given name
	 *
	 * 
	 */
	public FrameIndexSet(String name) {
		super();
		this.name = name;
	}
	
	/**
	 * Constructs a new FrameIndexSet object and loads data from an ArrayList
	 *
	 * 
	 */
	public FrameIndexSet(ArrayList<Integer> list) {
		frame_indices = new TreeSet<Integer>(list);
	}
	
	/**
	 * Constructs a new FrameIndexSet object of a given name and loads data from an ArrayList
	 *
	 * 
	 */
	public FrameIndexSet(ArrayList<Integer> list, String name) {
		frame_indices = new TreeSet<Integer>(list);
		this.name = name;
	}

	/**
	 * Constructs a new FrameIndexSet object and loads data from a TreeSet
	 *
	 * 
	 */
	public FrameIndexSet(TreeSet<Integer> set) {
		frame_indices = new TreeSet<Integer>(set);
	}
	
	/**
	 * Constructs a new FrameIndexSet object of a given name and loads data from a TreeSet
	 *
	 * 
	 */
	public FrameIndexSet(TreeSet<Integer> set, String name) {
		frame_indices = new TreeSet<Integer>(set);
		this.name = name;
	}
	
	/**
	 * Constructs a new FrameIndexSet object identical to a given FrameIndexSet
	 *
	 * 
	 */
	public FrameIndexSet(FrameIndexSet set){
		this.name = set.getName();
		frame_indices = new TreeSet<Integer>(set.frame_indices);
	}
	
	// Getters and setters

	/**
	 * Returns the name of frame index set
	 *
	 * @return Name of frame index set
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of frame index set
	 *
	 * 
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns frame index set as a TreeSet
	 *
	 * @return frame index set as a TreeSet
	 */
	public TreeSet<Integer> getAsTreeSet(){
		return frame_indices;
	}

	/**
	 * Returns frame index set as an ArrayList
	 *
	 * @return frame index set as an ArrayList
	 */
	public ArrayList<Integer> getAsArrayList(){
		return new ArrayList<Integer>(frame_indices);
	}

	/**
	 * Returns the number of frames in this frame index set
	 *
	 * @return number of frames
	 */
	public int getNumberOfFrames(){
		return frame_indices.size();
	}
	
	/**
	 * Returns true if the frame index set contains a given frame index
	 *
	 * 
	 */
	public boolean isFrameIn(int frame_index){
		return frame_indices.contains(frame_index);
	}
	
	// Modifications
	
	/**
	 * Adds a new frame to the frame index set
	 *
	 * 
	 */
	public void addFrame(int frame_index){
		frame_indices.add(frame_index);
	}
	
	/**
	 * Removes a frame from the frame index set
	 *
	 * 
	 */
	public void removeFrame(int frame_index){
		frame_indices.remove(frame_index);
	}
		
	// Set operations
	
	/**
	 * Returns the intersection of this frame index set and another
	 *
	 * @return intersection of two frame index sets
	 */
	public FrameIndexSet intersect(FrameIndexSet other) {
	TreeSet<Integer> ret = getAsTreeSet();
	ret.retainAll(other.getAsTreeSet());
	return new FrameIndexSet(ret,"Intersection("+getName()+","+other.getName()+")");
	}

	/**
	 * Returns the subtraction of another frame index set from this frame index set
	 *
	 * @return subtraction of two frame index sets
	 */
	public FrameIndexSet subtract(FrameIndexSet other) {
		TreeSet<Integer> ret = new TreeSet<Integer>(getAsTreeSet());
		ret.removeAll(other.getAsTreeSet());
		return new FrameIndexSet(ret,"Subtraction("+getName()+","+other.getName()+")");
	}

	/**
	 * Returns the union of this frame index set and another
	 *
	 * @return union of two frame index sets
	 */
	public FrameIndexSet union(FrameIndexSet other) {
		TreeSet<Integer> ret = getAsTreeSet();
		ret.addAll(other.getAsTreeSet());
		return new FrameIndexSet(ret,"Union("+getName()+","+other.getName()+")");
	}
	
	// toString, equals and clone
	/**
	 * Returns the String representation of frame index set
	 *
	 * @return String representation
	 */
	public String toString(){
		StringBuffer buf = new StringBuffer("[ "+getName()+" ]");
		Iterator<Integer> itr = this.getAsTreeSet().iterator();
		int i=0;
		while (itr.hasNext()) {
			if (i%15==0) buf.append(" \n");
			else buf.append(" ");
			String numstr = String.valueOf(itr.next());
			if (numstr.length()==3) numstr = " "+numstr;
			if (numstr.length()==2) numstr = "  "+numstr;
			if (numstr.length()==1) numstr = "   "+numstr;
			buf.append(numstr);
			i++;
		}
		return buf.toString();
	}
	
	/**
	 * Returns summary information about the frame index set
	 *
	 * @return summary information
	 */
	public String toStringInfo(){
		return "Name: "+name+" | ("+getNumberOfFrames()+" frame_indices)";
	}

	/**
	 * Returns true if this frame index set is identical to another
	 *
	 * 
	 */
	public boolean equals(Object other){
		if (other == null) return false;
		if (other == this) return true;
		if (this.getClass() != other.getClass()) return false;
		FrameIndexSet group = (FrameIndexSet)other;
		return group.frame_indices.equals(frame_indices)&&group.name.equals(name);
	}
	
	/**
	 * Returns hash code
	 * 
	 */
	public int hashCode() {
		return frame_indices.size()+name.length();
	}
	
	
	/**
	 * Returns an identical FrameIndexSet object 
	 *
	 * @return clone of the frame index set
	 */
	public Object clone(){
		try {
			FrameIndexSet ret = (FrameIndexSet)super.clone();
			ret.name = name;
			ret.frame_indices = new TreeSet<Integer>();
			Iterator<Integer> itr = frame_indices.iterator();
			while(itr.hasNext()) ret.frame_indices.add(itr.next());
			return ret;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
