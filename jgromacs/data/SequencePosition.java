/**
 * 
 * Written by M�rton M�nz and Philip C Biggin
 * Copyright (c) University of Oxford, United Kingdom
 * Visit http://sbcb.bioch.ox.ac.uk/jgromacs/
 * 
 * This source code file is part of JGromacs v1.0.
 * 
 * JGromacs v1.0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JGromacs v1.0. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JGromacs v1.0. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package jgromacs.data;

import jgromacs.db.ResidueType;

/**
 * Objects of this class represent a single sequence position
 *
 */
public class SequencePosition implements Cloneable {

	private ResidueType residueType = new ResidueType();
	private int index = 0;
	private String annotation = "";
	
	// Constructors
	/**
	 * Constructs a new SequencePosition object
	 * 
	 * 
	 */
	public SequencePosition() {
		
	}
	
	/**
	 * Constructs a new SequencePosition object of given index and residue type
	 * @param index index
	 * @param residueType residue type
	 * 
	 */
	public SequencePosition(int index, ResidueType residueType){
		setIndex(index);
		this.residueType = residueType;
	}
	
	/**
	 * Constructs a new SequencePosition object of given index and annotation
	 * @param index index
	 * @param annotation annotation
	 * 
	 */
	public SequencePosition(int index, String annotation) {
		super();
		this.index = index;
		this.annotation = annotation;
	}

	/**
	 * Constructs a new SequencePosition object of residue type and annotation
	 * @param residueType residue type
	 * @param annotation annotation
	 * 
	 */
	public SequencePosition(ResidueType residueType, String annotation){
		this.residueType = residueType;
		setAnnotation(annotation);
	}
	
	/**
	 * Constructs a new SequencePosition object of given index, residue type and annotation
	 * @param index index
	 * @param residueType residue type
	 * @param annotation annotation
	 * 
	 */
	public SequencePosition(int index, ResidueType residueType, String annotation){
		setIndex(index);
		this.residueType = residueType;
		setAnnotation(annotation);
	}

	// Getters and setters	
	
	/**
	 * Returns the index of sequence position
	 *
	 * @return index of sequence position
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Sets the index of sequence position
	 * @param index index of sequence position
	 * 
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * Returns the annotation of sequence position
	 *
	 * @return annotation of sequence position
	 */
	public String getAnnotation(){
		return annotation;
	}
	
	/**
	 * Sets the annotation of sequence position
	 * @param annotation annotation of sequence position
	 * 
	 */
	public void setAnnotation(String annotation){
		this.annotation = annotation;
	}

	/**
	 * Returns the residue type of sequence position
	 *
	 * @return residue type of sequence position
	 */
	public ResidueType getResidueType() {
		return residueType;
	}

	/**
	 * Sets the residue type of sequence position
	 * @param residueType residue type of sequence position
	 * 
	 */
	public void setResidueType(ResidueType residueType) {
		this.residueType = residueType;
	}

	// clone, equals, toString
	
	/**
	 * Returns an identical SequencePosition object 
	 *
	 * @return clone of the sequence position
	 */
	public Object clone(){
		 try {
			SequencePosition ret = (SequencePosition)super.clone();
			ret.index = index;
			ret.annotation = annotation;
			ret.residueType = (ResidueType)residueType.clone();
			return ret;
		 } catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	/**
	 * Returns true if the two sequence positions are identical
	 * @param other the other sequence position
	 * 
	 */
	public boolean equals(Object other){
		if (other == null) return false;
		if (other == this) return true;
		if (this.getClass() != other.getClass()) return false;
		SequencePosition sp = (SequencePosition)other;
		if (residueType.equals(sp.residueType)&&(index==sp.index)&&(annotation.equals(sp.annotation))) 
		return true;
		else return false;
	}
	
	/**
	 * Returns hash code
	 * 
	 */
	public int hashCode() {
		return index+annotation.length();
	}
	
	
	/**
	 * Returns the String representation of sequence position
	 *
	 * @return String representation
	 */
	public String toString(){
		return index+residueType.get3LetterCode();
	}
	
}

