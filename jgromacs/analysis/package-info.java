/**
 * jgromacs.analysis is a collection of classes providing static methods for various analysis tasks
 */
package jgromacs.analysis;