/**
 * 
 * Written by M�rton M�nz and Philip C Biggin
 * Copyright (c) University of Oxford, United Kingdom
 * Visit http://sbcb.bioch.ox.ac.uk/jgromacs/
 * 
 * This source code file is part of JGromacs v1.0.
 * 
 * JGromacs v1.0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JGromacs v1.0. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JGromacs v1.0. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package jgromacs.ui;

/**
 * Template JGromacs application
 *
 */
public class TemplateApplication extends Application{

		
	public TemplateApplication(){
		super();
		// Your XML configuration file:
		XMLFileName = "template.xml"; 
	}
	
	public void runCore() {
		
		// Put your code here...
		// Use runCore() as you normally use the main() method!
		// It is the entry point of your program.
		
		// Accessing attributes is simple...
		
		// You can access the value corresponding to a given flag:
		String c_value = getArgumentValue("c");
		System.out.println("Value assigned to flag -c was "+c_value);
		
		// You can ask if a flag was given in the attributes: 
		boolean is_c_given = isArgumentGiven("c");
		if (is_c_given) System.out.println("Flag -c was given in the attributes");
		else System.out.println("Flag -c was NOT given in the attributes");
	
	}
	
	public static void main(String[] args) {
		// Don't modify!
		TemplateApplication app = new TemplateApplication();
		app.run(args);
	}

}
